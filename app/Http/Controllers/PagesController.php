<?php

namespace App\Http\Controllers;

use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PagesController extends Controller
{
    use \App\Traits\Email;
    public $_settings = [];

    public function __construct()
    {
        $this->_settings = Models\Setting::customFetchAll();
        
        View::share('settings', $this->_settings);

    }


    public function index()
    {   
        return view('pages.index');
    }
}