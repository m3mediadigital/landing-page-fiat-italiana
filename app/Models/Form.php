<?php

namespace App\Models;

class Form extends AppDescomplicarModel
{
	protected $table = 'forms';

    public static function customGetBySlug($slug)
    {
    	// $dados = self::customAll($slug);
    	$query = self::query();
        $query->where([
            ['companies_id', self::$COMPANY_ID],
            ['slug', $slug]
        ]);
        $form = $query->first();
        return $form;
    }
}
