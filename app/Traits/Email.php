<?php

namespace App\Traits;

use App\Models;
use App\Mail\Contato;
use App\Models\Coupon;
use App\Models\Showroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use nusoap_client;

trait Email
{
    private function &getValidator($inputs, $rules = [])
    {
        Validator::extend('phone_br', function ($attribute, $value, $parameters, $validator) {
            
            function valida($value){

                //Retorna apenas os dígitos do telefone (barra caractere especial)
                $number = preg_replace('/([^\d]+)/', '', $value);

                //Retorna a quantidade de dígitos do telefone
                $qnt_dig = strlen($number);

                //Retornao código de área (DDD)
                $cod_area = substr($number, 0, 2);

                //Retorna primeiro dígito depois do código de área
                $number_first = substr($number, 2, 1);

                //Dígitos inicias permitidos para telefone fixo (length == 10)
                $numbers_first_fixo = ['2', '3', '4', '5'];

                //Dígitos inicias permitidos para telefone fixo (length == 11)
                $numbers_first_cel = ['7', '8', '9'];

                //dd($number, $qnt_dig, $cod_area, $number_first);

                //Array com todos os códigos de área válidos
                $ddds = ["68", "82", "96", "97", "92", "71", "73", "74", "75", "77", "85", "88", "27", "28", "61", "62", "64", "98", "99", "65", "66", "67", "31", "32", "33", "34", "35", "37", "38", "91", "93", "94", "83", "41", "42", "43", "44", "45", "46", "81", "87", "86", "89", "21", "22", "24", "84", "51", "53", "54", "55", "69", "95", "47", "48", "49", "11", "12", "13", "14", "15", "16", "17", "18", "19", "79", "63"];

                // Números inválidos
                $blocks = ["22222222", "33333333", "44444444", "55555555", "777777777", "888888888", "999999999", "23456789", "34567890", "987654321", "876543210"];

                //Valida a quantidade de dígitos
                if($qnt_dig < 10 || $qnt_dig > 11) {
                    return "Número inválido! É necessário ter no mínimo 10 digitos (incluído o DDD)";
                } else {

                    if(!in_array($cod_area, $ddds)) {
                        return "O código de área informado não existe.";
                    }

                    if($qnt_dig == 10) {
                        // Valida o primeiro dígito do fixo
                        if(!in_array($number_first, array_merge( $numbers_first_fixo, $numbers_first_cel))) {
                            return "Números fixos devem começar com 2, 3, 4 ou 5.";
                        } 
                        // Verifica se o número está na lista de $blocks. Se encontrar, barra
                        else if(in_array(substr($number, 2, 8), $blocks)) {
                            return "Fixo com todos dígitos repetidos ou em sequência (crescente ou decrescente), são inválidos";
                        }
                    }
                    
                    // Quando celular (lenght = 11, busca o primeiro dígito no array $numbers_first_cel).
                    else if($qnt_dig == 11) {
                        // Valida o primeiro dígito do celular
                        if(!in_array($number_first, array_merge( $numbers_first_fixo, $numbers_first_cel))) {
                            return "Números de celular devem começar com 7, 8 ou 9.";
                        }
                        // Verifica se o número está na lista de $blocks. Se encontrar, barra
                        else if(in_array(substr($number, 2, 9), $blocks)) {
                            return "Celular com todos dígitos repetidos ou em sequência (crescente ou decrescente), são inválidos";
                        }
                    }
                }
            }

            $retorno = valida($value);

            // Se quiser debugar a função
            //dd($retorno);

            //Se apresentar algum erro na função ele informará  telefone inválido
            return ($retorno == null);
        });

        Validator::extend('cpf', function ($attribute, $value, $parameters, $validator) {
            $onlyNumbers = preg_replace('/([^\d]+)/', '', $value);
            return (strlen($onlyNumbers) == 11);
        });

        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $secret = ENV('CAPTCHA_SECRET');

            $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$value."&remoteip=".$_SERVER['REMOTE_ADDR']), true);

            return ($response['success'] == true);
        });

        $messages = [
            'required' => 'O campo ":attribute" é obrigatório.',
            'required_if' => 'O campo ":attribute" é obrigatório.',
            'max' => 'O campo ":attribute" deve ter no máximo :max caracteres.',
            'phone_br' => 'O campo ":attribute" deve ser preenchido com DDD e o número com 8 ou 9 dígitos.',
            'present' => 'Você precisa confirmar o preenchimento correto do formulário.',
            'email' => 'O email informado é inválido.',
        ];

        $validator = Validator::make($inputs, $rules, $messages);
        return $validator;
    }

    private function getEmailsByKey(&$key)
    {
        // DEBUG
        // return 'diegom3@m3mediadigital.com.br';

        $emails = [];
        if (isset($this->_settings['email_' . $key]) && preg_match('/@/', $this->_settings['email_' . $key])) {
            $emails = preg_replace('/\s+/', '', $this->_settings['email_' . $key]);
            return explode(',', $emails);
        }
        
        if (isset($this->_settings['email_' . 'contato'])) {
            $emails = preg_replace('/\s+/', '', $this->_settings['email_' . 'contato']);
            return explode(',', $emails);
        }

        return $emails;
    }

    private function sendEmail(&$request, &$rules, $keySettingSuffix, $subject, $layout = null, $enviarLead = true)
    {
        $requestData = $request->all();

        $validator = $this->getValidator($requestData, $rules);

        if ($validator->fails()) {

            $request->session()->flash('flash.error', $validator->errors()->first());
            return 'error';

        } else {

            $mailable = new Contato($requestData, $layout);
            $mailable->subject($subject);

            if (!empty($request->files)) {
                foreach ($request->files as $field => $file) {
                    $mailable->attach($file->getRealPath(), [
                        'as' => $file->getClientOriginalName(),
                        'mime' => $file->getMimeType()
                    ]);
                }
            }

            $emails = $this->getEmailsByKey($keySettingSuffix);

            $send_copy = $request->get('copy_email');

            try {
                if(isset($send_copy) && $send_copy == "true"){
                    $mailable_copy = new Contato($requestData, $layout);
                    $mailable_copy->subject($subject);
                    Mail::to($request->get('email'))->send($mailable_copy);
                }

                Mail::to($emails)->send($mailable);

            } catch (\Exception $e) {
                //dump($e);
                //return ['flash.error', 'Houve um problema com o envio... Tente novamente.'];

            } finally {
                if($enviarLead == true) {
                    $a = $this->postLeadsDescomplicar('campanha', $requestData, $mailable, $request->files);
                    // $this->send_syonet($request, $loja);
                }
            }

            $request->session()->flash('flash.success', 'Sua mensagem foi enviada com sucesso!');
            return 'success';

        }
    }

    private function getFormSlug(&$key)
    {

        // if( isset($this->_settings['email_'.$key]) )
        //     return $key;
        // return 'contato';

        if (class_exists("\\App\\Models\\Form")) {
            $form = \App\Models\Form::customGetBySlug($key);
            if (!empty($form))
                return $form->slug;
        }

        return null;
    }


    private function postLeadsDescomplicar($keySettingSuffix, $requestData, &$mailable, &$files = null)
    {
        $form = $this->getFormSlug($keySettingSuffix);
        // dd($form);
        if (!empty($form)) {

            // $urlGetFields = 'http://api.carros.novam3.com/leads/getFields?key='.$this->carros_key.'&form='.$form;
            $url = 'http://api.carros.novam3.com/leads/insertLead?key=' . \App\Models\AppDescomplicarModel::$COMPANY_KEY . '&form=' . $form;
            $dados = [];
            
            foreach (['nome', 'email', 'telefone'] as $field) {
                if (!empty($requestData[$field]) && !empty(trim($requestData[$field]))) {
                   $dados[$field] = $requestData[$field];
                    unset($requestData[$field]);
                }
                
            }

            $view = \Illuminate\Support\Facades\View::make($mailable->layout, ['requestData' => $requestData]);

            // dd($requestData);
            
            $dados['mensagem'] = $view->render();

            if (empty(trim($dados['mensagem'])))
                unset($dados['mensagem']);

            $dados = http_build_query($dados);
            // dd($dados);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);
        }
    }

    private function testSyonetRequest(&$sClient, &$response)
    {
        if( $response == null )
            $response = new \stdclass();
        $response->debug = [];
        $error = $sClient->getError();

        if ($error) {
            $response->debug['error'] = $error;
        }

        if ($sClient->fault) {
            $response->debug['fault'] = true;
        }

        return $response;

    }

    private function send_syonet($request, $targetLoja = null) {
        $result = array();
        $result_prudente = array();
        $result_br = array();

        $client = @new \nusoap_client("http://crm.nacionalvw.com.br:8888/CollaborativeWS-CollaborativeWS/CollaborativeEventoService", false, '', '', '', '');

        $err = $client->getError();
        if ($err) {
            $result = $err;
        } else {
            $client->setUseCurl('0');

            $numbers = preg_replace("/[^0-9]/", "", $request->input('telefone'));
            $ddd  = substr($numbers, 0, 2);
            $fone = substr($numbers, 2);

            $grupoEvento = $request->grupoEvento ? $request->grupoEvento : 'OPORTUNIDADE';

            $tipoEvento  = $request->tipoEvento != null ? $request->tipoEvento : 'WEB';


            // comentando condicao devido a solicitacao
            // if ($ddd != '84') {
            //     $result = array(
            //         'codigo' => 1,
            //         'mensagem' => 'Apenas DDDs do estado do Rio Grande do Norte são permitidos.',
            //     );
            // } else {
                $obs = [];
                if( !empty($request->input('origem')) )
                    $obs[] = $request->input('origem');
                if( !empty($request->input('veiculo')) )
                    $obs[] = $request->input('veiculo');
                if(!empty($request->input('forma_pagamento')))
                    $obs[] = $request->input('forma_pagamento');
                if(!empty($request->input('parcelas')))
                    $obs[] = $request->input('parcelas');
                if(!empty($request->input('entrada')))
                    $obs[] = $request->input('entrada');
                if(!empty($request->input('mensagem')))
                    $obs[] = $request->input('mensagem') . " | ";

                $dados['usuario'] = 'site';
                $dados['senha'] = 'novam3';
                $dados['dominio'] = 'nacional';
                $dados['grupoEvento'] = $grupoEvento;
                $dados['tipoEvento'] = $tipoEvento;
                $dados['nome'] = !empty($request->input('nome')) ? $request->input('nome') : '';

                if( filter_var($request->input('email'), FILTER_VALIDATE_EMAIL) !== false){
                    $dados['email'] = $request->input('email');
                }

                $dados['dataNascimento'] = '';
                $dados['dddResidencial'] = $ddd;
                $dados['telefoneResidencial'] = $fone;
                $dados['dddComercial'] = '';
                $dados['telefoneComercial'] = '';
                $dados['dddCelular'] = '';
                $dados['telefoneCelular'] = '';
                $dados['tipoLogradouro'] = '';
                $dados['logradouro'] = '';
                $dados['numero'] = '';
                $dados['bairro'] = '';
                $dados['cep'] = '';
                $dados['cidade'] = '';
                $dados['estado'] = '';
                $dados['assunto'] = !empty($request->input('veiculo')) ? $request->input('veiculo') : $request->input('assunto');
                //$dados['assunto'] = !empty($request->input('assunto')) ? $request->input('assunto') : '';
                $dados['observacao'] = utf8_decode(implode(' | ', $obs));
                $dados['novoUsado'] = !empty($request->input('novoUsado')) ? $request->input('novoUsado') : 'NOVO';
                $dados['midia'] = '999';
                $dados['palavraChave'] = '';
                $dados['idUsuario'] = '0';
                $dados['idCliente'] = '0';
                $dados['cpfCnpj'] = '0';
                $dados['idEvento'] = '0';
                $dados['status'] = '';
                $dados['classificacao'] = '';
                $dados['prioridade'] = '';
                $dados['diasParaAlteracaoEmEventoAberto'] = 1095;

                if ($targetLoja == 'Nacional BR 101' || is_null($targetLoja)) {
                    $dados['idEmpresa'] = '5';
                    $apiBrResult = $client->call('gerarEventoV2', $dados, 'http://evento.ws.action.syonet.com.br/', 'http://evento.ws.action.syonet.com.br/');
                    $result_br = json_decode($apiBrResult);
                }else{
                    $result_br = null;
                }

                if($targetLoja == 'Nacional Prudente' || is_null($targetLoja)){
                    $dados['idEmpresa'] = '1';
                    $apiPrudenteResult = $client->call('gerarEventoV2', $dados, 'http://evento.ws.action.syonet.com.br/', 'http://evento.ws.action.syonet.com.br/');
                    $result_prudente = json_decode($apiPrudenteResult);
                }else{
                    $result_prudente = null;
                }

                // TODO: Capturar erros caso a string de lojas mude

                $this->testSyonetRequest($client, $result_br);
                $this->testSyonetRequest($client, $result_prudente);
        }
    }

    public function formularioLanding(Request $request)
    {
        $request->grupoEvento = 'WEB';
        $request->tipoEvento = 'VEICULOS NOVOS';
        $rules = [
            'nome' => 'required|max:255',
            'telefone' => 'required|phone_br',
            'email' => 'required|email'
        ];
        // dd($request->all());

        $msg = $this->sendEmail($request, $rules, 'leads', 'Mensagem enviada através do formulário de Contato');
        if($msg == true)
            return redirect()->back()->with('success','Sua mensagem foi enviada com sucesso!');
        else
            return redirect()->back()->with('error','Erro, tente novamente!');
        // if ($msg == 'success') {
        //     return response()->json([
        //         'send' => 'true',
        //         'message' => 'Sua Mensagem foi enviada com sucesso! <br> ',
        //         'error' => ""
        //     ]);
        // } else {
        //     return response()->json([
        //         'send' => 'false',
        //         'message' => 'Houve um erro ao tentar enviar seu email',
        //     ]);
        // }

    }

}