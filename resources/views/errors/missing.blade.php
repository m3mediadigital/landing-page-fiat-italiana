<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env("APP_NAME", "SITE") }} - 404</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            @font-face {
                font-family: 'VW-Regular';
                src: url('/fonts/VWTextWeb-Regular.eot');
                src: url('/fonts/VWTextWeb-Regular.eot?#iefix') format('embedded-opentype'),
                url('/fonts/VWTextWeb-Regular.woff2') format('woff2'),
                url('/fonts/VWTextWeb-Regular.woff') format('woff'),
                url('/fonts/VWTextWeb-Regular.ttf') format('truetype');
                font-weight: normal;
            }

            html, body {
                color: #fff;
                font-family: 'VW-Regular', 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                background-image: url('/images/bg-vw.jpg');
                background-color: #19C6F4;
                background-repeat: no-repeat;
                background-size: cover;
                background-position: 50%;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
                z-index: 5;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-family: sans-serif;
                font-size: 84px;
                color: #fff;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .subtitle{
                font-weight: bold;
                font-size: 24px;
            }
            .mask-transparent {
                background: linear-gradient(to top, rgba(0, 0, 0, 1), transparent);
                position: absolute;
                width: 100%;
                height: 100%;
                z-index: 4;
            }
        </style>
    </head>
    <body>
    <div class="mask-transparent"></div>
        <div class="flex-center position-ref full-height" style=" ">


            <div class="content">
                <div class="title m-b-md">
                <img src="/images/logo-white.png">
                </div>
                <div class="title m-b-md">
                  404
                </div>
                <div class="subtitle m-b-md ">
                    A página que você procura não foi encontrada <br>
                    encontre o <strong>Volkswagen</strong> perfeito para você nos seguintes links
                </div>
                <div class="links">
                    <a href="{{ route('/') }}">Home</a>
                    <a href="{{ route('ofertas') }}">Ofertas</a>
                    <a href="{{ route('novos') }}">Novos</a>
                    <a href="{{ route('seminovos') }}">Seminovos</a>
                    <a href="{{ route('consorcios') }}">Consórcios</a>
                    <a href="{{ route('vendas-diretas') }}">Vendas Diretas</a>
                </div>
            </div>
        </div>
    </body>
</html>
