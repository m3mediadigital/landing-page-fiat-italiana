<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

{{-- Por algum motivo desconhecido, não consegui importar o js dentro do boostrap.js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.3.2/sweetalert2.min.js" async defer></script>


@yield('js')