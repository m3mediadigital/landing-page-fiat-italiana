<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppDescomplicarModel extends Model
{
    const ACTIVE = 1;
    
    static $COMPANY_ID = 40; // Fiat italiana
    static $COMPANY_KEY = 'c3771c28baec9490756ba9729f4e2981';

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        if(!self::$COMPANY_ID || !is_int(self::$COMPANY_ID)) {
            throw new \Exception("O ID da empresa informado é inválido.");
        }
    }

    protected static function singleCustomKey(&$object)
    {
        if($object && !empty($object->custom_answers)) {
            foreach($object->custom_answers as $k => $custom_answer) {
                $campo = $custom_answer->custom_question->slug;

                if($custom_answer->custom_question->type == 'select') {
                    $option = CustomOption::query()
                        ->select('custom_option')
                        ->where([
                            ['custom_options.custom_questions_id', '=', $custom_answer->custom_question->id],
                            ['custom_options.id', '=', $custom_answer->value],
                        ])
                        ->first();
                    $object->$campo = $option->custom_option;

                } else if(in_array($custom_answer->custom_question->type, ['checkbox', 'select_image'])) {
                    $pre = '';
                    if($custom_answer->custom_question->type == 'select_image') {
                        $pre = 'https://carros.novam3.com/upload/';
                    }
                    $option = CustomOption::query()
                        ->select('custom_option')
                        ->where('custom_options.custom_questions_id', '=', $custom_answer->custom_question->id)
                        ->whereIn('custom_options.id', unserialize($custom_answer->value) )
                        ->get();
                    $opts = array();
                    foreach($option as $opt) {
                        $opts[] = $pre . $opt->custom_option;
                    }
                    $object->$campo = $opts;

                } else if(in_array($custom_answer->custom_question->type, ['image', 'album_images']) && trim($custom_answer->value) != '') {
                    $object->$campo = 'https://carros.novam3.com/upload/' . $custom_answer->value;

                } else if($custom_answer->custom_question->type == 'money') {
                    $money = str_replace('.', '', $custom_answer->value);
                    $money = str_replace(',', '.', $money);
                    $object->$campo = (float)$money;
                    
                } else {
                    $object->$campo = $custom_answer->value;
                }
            }

            unset($object->custom_answers);
        }

        $optionals_array = [];
        if($object && !empty($object->optionals)) {
            foreach($object->optionals as $k => $opt) {
                $slug = mb_convert_case( preg_replace('/(\s+)/', '-', $opt->value), MB_CASE_LOWER, 'utf-8' );
                // Falta tirar acentos e caracteres especiais...
                $object->optionals[$k]->slug = $slug;
                $optionals_array[ $object->optionals[$k]->slug ] = $opt->value;
            }
            unset($object->optionals);
        }
        $object->optionals = $optionals_array;

    }

    protected static function genericCustomPaginate(&$query, $order = [], $limit = null, $page = null)
    {

        if($order){
            if( is_array($order) && count($order) == 2 ){
                list($field, $ord) = $order;
                $query->orderBy($field, $ord);
            }else if( $order == 'RAND' ){
                $query->inRandomOrder();
            }
        }

        if($limit && is_numeric($limit)) {
            $query->limit($limit);
            
            if($page && is_numeric($page)) {
                $query->offset( (($page-1)*$limit) );
            }
        }

    }

}