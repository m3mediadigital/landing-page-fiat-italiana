<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Session;

/**
* 
*/
class FlashModal extends AppHelper
{
	// JS => $(".flash-render").modal();
    public static function flash($flashClass, $type = null, $message = null)
    {
		if( empty($type) ){
			if( Session::has('flash.error') )
				$type = 'error';
			else if( Session::has('flash.success') )
				$type = 'success';
		}

    	ob_start();
		
		if( !empty($type) && class_exists($flashClass) ){

			if( empty($message) )
				$message = Session::pull('flash.'.$type, '');

	    	if( !empty($message) ){
	    		$slugHelper = preg_replace('/\\\\/', '-', strtolower($flashClass));
		    	?>
					<div class="modal fade flash-render <?= $type ?>" tabindex="-1" role="dialog">
					    <div class="modal-dialog" role="document">
					        <div class="modal-content">
					            <div class="modal-body text-center no-car">
					                <p class="title <?= $slugHelper ?>">
					                	<?= self::runRemoteFlash($flashClass, $type) ?>
					                </p>
					                <p class="text <?= $slugHelper ?>"><?= $message ?></p>
					            </div>
					        </div>
					    </div>
					</div>
		    	<?php
		    }
    	}

    	return ob_get_clean();
    }

    private static function runRemoteFlash($flashClass, $method)
    {
    	return ""; //call_user_func('\\' . $flashClass . '::' . $method);
    }
}
