require("./bootstrap");

var SPMaskBehavior = function(val) {
        return val.replace(/\D/g, "").length === 11 ? "(00) 00000-0000" : "(00) 0000-00009";
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

$(".sp_celphones").mask(SPMaskBehavior, spOptions);

function validField(name, param) {
    var has_error = false;
    var value;
    if (param.is("select")) {
        value = param.find("option:selected").val();
    } else {
        value = param.val();
    }

    if (name == "Email") {
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) has_error = true;
    } else if (name == "Telefone" || name == "Celular" || name == "Whatsapp") {
        if (value.length < 14 || value == "") has_error = true;
    } else if (name == "cpf") {
        value = value.replace(/\D/g, "");
        if (!TestaCPF(value)) has_error = true;
    } else if (name == "Entrada") {
        if (value.length < 6 || value == "R$ ") has_error = true;
    } else {
        if (value == undefined || value.length == 0 || value == "") {
            has_error = true;
        }
    }

    if (has_error) {
        param.parent(".form-group").addClass("group-error");
        param.parent(".form-group").removeClass("group-success");
    } else {
        param.parent(".form-group").removeClass("group-error");
        param.parent(".form-group").addClass("group-success");
    }
    return has_error;
}

function tratamentoBotao(botao, ativo, texto = false) {
    if (ativo) {
        $(botao).addClass("btn-aguarde");
        $(botao).prop("disabled", true);
        $(botao).text("AGUARDE");
    } else {
        $(botao).removeClass("btn-aguarde");
        $(botao).prop("disabled", false);
        $(botao).text(texto ? texto : "Enviar");
    }
}

// $(".contact-form").submit(function(event) {
//     var nome = $("#nome");
//     var email = $("#email");
//     var telefone = $("#telefone");

//     if (validField("Telefone", telefone) | validField("Nome", nome) | validField("Email", email)) {
//         $(this)
//             .find(".btn-schedule")
//             .addClass("btn-group-error");
//         $(".form-scheduling")
//             .find('select[name="servico"]')
//             .prop("disabled", true);
//         event.preventDefault();
//     }
// });

$("#contactForm").submit(function() {
    console.log("oiuytr");
    var nome = $("#nome");
    var email = $("#email");
    var telefone = $("#telefone");
    var origem = $("#origem");
    var _token = $("input[name=_token]");

    if (validField("Telefone", telefone) | validField("Nome", nome) | validField("Email", email)) {
        $(this)
            .find(".btn-schedule")
            .addClass("btn-group-error");
        $(".form-scheduling")
            .find('select[name="servico"]')
            .prop("disabled", true);
        event.preventDefault();
    }
    // else {
    //     $.ajax({
    //         type: "POST",
    //         url: "/formulario-landing",
    //         data: {
    //             nome: nome,
    //             email: email,
    //             telefone: telefone,
    //             origem: origem,
    //             _token: _token
    //         }
    //     });
    // }
    //         // success: function(result) {
    //     console.log($data);
    //     if (data.send == "true") {
    //         // submit_button.html("Enviado!");
    //         // console.log("Enviado");
    //         // // var success_sweet = "<script> swal('Sucesso!','" + data.message + "','success' )</script>";
    //         // // $("#success-message").html(success_sweet);
    //         $("#contactForm")[0].reset();
    //         submitMSG(true, resultado.message);
    //     } else {
    //         // submit_button.html("Enviar");
    //         // submit_button.attr("disabled", false);
    //         // console.log("error");
    //         // var error_sweet = "<script> swal( 'Erro!','" + data.message + "','error' )</script>";
    //         // $("#error-message").html(error_sweet);
    //         formError();
    //         submitMSG(false, resultado.message);
    //     }
    // },
    // datatype: "json"
    // });
    // console.log($(this).serialize());
    // return false;
    // }
});

function formSuccess() {
    $("#contactForm")[0].reset();
    submitMSG(true, "You mail has been sent successfully!");
}
function formError() {
    $("#contactForm")
        .removeClass()
        .addClass("animated")
        .one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function() {
            $(this).removeClass();
        });
}
function submitMSG(valid, msg) {
    if (valid) {
        var msgClasses = "text-success";
    } else {
        var msgClasses = "text-danger";
    }
    $("#msgSubmit")
        .removeClass()
        .addClass(msgClasses)
        .html(msg);
}

(function($) {
    "use strict";

    // Preloader
    jQuery(window).on("load", function() {
        preloader();
    });

    $("a").on("click", function(event) {
        if (this.hash !== "") {
            event.preventDefault();

            var hash = this.hash;

            $("html, body").animate(
                {
                    scrollTop: $(hash).offset().top
                },
                800,
                function() {
                    window.location.hash = hash;
                }
            );
        }
    });

    // Animation section
    if (jQuery(".wow").length) {
        var wow = new WOW({
            boxClass: "wow", // animated element css class (default is wow)
            animateClass: "animated", // animation css class (default is animated)
            offset: 0, // distance to the element when triggering the animation (default is 0)
            mobile: true, // trigger animations on mobile devices (default is true)
            live: true // act on asynchronously loaded content (default is true)
        });
        wow.init();
    }

    // CounterUp
    jQuery(".counter").counterUp({
        delay: 10,
        time: 2000
    });

    // App Video
    function autoPlayYouTubeModal() {
        var trigger = $("body").find('[data-toggle="modal"]');
        trigger.on("click", function() {
            var theModal = $(this).data("target"),
                videoSRC = $("#video-modal iframe").attr("src"),
                videoSRCauto = videoSRC + "?autoplay=1";
            $(theModal + " iframe").attr("src", videoSRCauto);
            $(theModal + " button.close").on("click", function() {
                $(theModal + " iframe").attr("src", videoSRC);
            });
            $(".modal").on("click", function() {
                $(theModal + " iframe").attr("src", videoSRC);
            });
        });
    }
    autoPlayYouTubeModal();

    // Team Member Carousel
    jQuery("#team-members").owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        margin: 30,
        nav: false,
        smartSpeed: 500,
        dots: true,
        autoplayHoverPause: false,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    // Testimonials Carousel
    jQuery("#testimonial-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        margin: 30,
        nav: false,
        smartSpeed: 500,
        dots: true,
        autoplayHoverPause: false,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    // Screenshot Carousel
    jQuery("#app-screen").owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        margin: 30,
        nav: false,
        smartSpeed: 500,
        dots: true,
        autoplayHoverPause: false,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            576: {
                items: 2
            },
            768: {
                items: 2
            },
            992: {
                items: 3
            }
        }
    });

    // Preload
    function preloader() {
        jQuery(".preloaderimg").fadeOut();
        jQuery(".preloader")
            .delay(200)
            .fadeOut("slow")
            .delay(200, function() {
                jQuery(this).remove();
            });
    }

    jQuery(".phone-mockup img").tilt({
        maxTilt: 6
    });

    // Vertical Center Modal
    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = jQuery(".modal-vcenter:visible");
        }
        $modals.each(function(i) {
            var $clone = jQuery(this)
                .clone()
                .css("display", "block")
                .appendTo("body");
            var top = Math.round(($clone.height() - $clone.find(".modal-content").height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            jQuery(this)
                .find(".modal-content")
                .css("margin-top", top);
        });
    }

    jQuery(".modal-vcenter").on("show.bs.modal", function(e) {
        centerModals($(this));
    });
    jQuery(window).on("resize", centerModals);
})(jQuery);
