try {
    window.$ = window.jQuery = require('jquery');

    require('owl.carousel');
    require('jquery-mask-plugin');
    require('@fancyapps/fancybox');
    require("./jquery.counterup.min");
    require("./jquery.easing.min");
    require("./popper.min");
    require("./SmoothScroll");
    require("./tilt.jquery");
    require("./waypoints.min");
    require("./form-validator.min");
    require("./script");

} catch (e) {}
