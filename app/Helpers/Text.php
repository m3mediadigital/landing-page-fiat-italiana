<?php

namespace App\Helpers;

/**
* 
*/
class Text extends AppHelper
{
	public static function sequence($mixed, $sep=', ', $last=" e ")
	{
		if( is_array($mixed) ){
			$lastIdx = count($mixed)-1;
			if( $lastIdx > 1 ){
				$buffer = implode($sep, $mixed);
				$lastChar = array_pop($mixed);
				return preg_replace('/'.$sep.$lastChar.'$/', $last.$lastChar, $buffer);
			}else if( $lastIdx >= 0 ){
				return implode($last, $mixed);
			}
		}else if( is_string($mixed) && !empty($mixed) ){
			return $mixed;
		}
		return '';
	}

	public static function clean($text)
	{
		return trim( str_replace('&nbsp;', ' ', strip_tags($text)) );
	}

	public static function clause($text)
	{
		return ucfirst(strtolower( self::clean($text) ));
	}

	public static function slugify($text)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
			return 'n-a';
		}

		return $text;
	}
}
