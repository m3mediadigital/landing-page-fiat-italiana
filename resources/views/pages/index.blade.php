@extends('pages.layout')
@section('content')
    <!--Preload-->
    <div class="preloader">
        <div class="preloader_image">
            <div class="sk-double-bounce">
                <div class="sk-child sk-double-bounce1"></div>
                <div class="sk-child sk-double-bounce2"></div>
            </div>
        </div>
    </div>
    <!--end Preload-->
    
    <section class="">
        <a href="#contact"><img src="{{ asset('images/comparativo-fiat-italiana.png')}}" alt="Landing" class="img-fluid top-landing"></a>
    </section>
    <!-- end home -->
    
    <section id="contact" class="section-spacing white-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center">
                        <h2>Argo - Edição Especial!</h2>
                        <p>Informe corretamente os dados abaixo.</p>
                        @if(session()->get('success'))
                            <div class="alert d-flex justify-content-center">
                                <h4 class="alert-success"><i class="icon fa fa-check"></i> {{ session()->get('success') }}</h4>                                
                            </div>
                        @endif

                        @if(session()->get('error'))
                            <div class="alert d-flex justify-content-center">
                                <h4 class="alert-danger"><i class="icon fa fa-ban"></i> {{ session()->get('error') }}</h4>                                
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <form id="contactForm" action="{{ route('formularioLanding') }}" class="contact-form wow zoomIn contactForm" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" id="origem" name="origem" value="Campanha Cronos Edição especial">                      
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input placeholder="Nome" id="nome" class="form-control" name="nome" type="text" data-error="Digite seu nome">
                                    <small class="msg-success"></small>
                                    <small class="msg-error-red">Digite seu nome</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input placeholder="Email" id="email" class="form-control" name="email" type="email" data-error="Digite seu e-mail"> 
                                    <small class="msg-success"></small>
                                    <small class="msg-error-red">Digite seu email</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input placeholder="Telefone" id="telefone" class="form-control sp_celphones" name="telefone" type="text" data-error="Digite seu telefone">
                                    <small class="msg-success"></small>
                                    <small class="msg-error-red">Digite seu telefone</small>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <input value="Cadastrar" name="submit" class="btn btn-primary float-right" type="submit">
                            </div>
                            <div class="col-md-6 offset-md-6 d-flex justify-content-end">
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
            
        </div>
    </section>
    <!-- end contact us -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo text-center">
                        <a href="#"><img src="{{ asset('images/fiat-italiana-logo.png') }}" width="360" alt="{{ $settings['site_title'] }}"></a>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="social-icons text-center">
                        <li><a href="{{ $settings['facebook'] }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="{{ $settings['instagram'] }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        {{-- <li><a href="https://api.whatsapp.com/send?phone=558481891313&text=Oi!" target="_blank"><i class="fa fa-whatsapp"></i></a> --}}
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="copyright-text text-center">
                        <p>Todos os direitos reservados.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>   
    <!-- Bact to top -->
    <div class="back-top">
        <a href="#"><i class="fa fa-angle-up"></i></a>
    </div>
    <!-- End Video Modal --> 
@endsection