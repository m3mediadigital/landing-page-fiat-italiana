<?php

namespace App\Helpers;

/**
* 
*/
class Setting extends AppHelper
{

	public static function has(&$settings, $key)
	{
		return ( !empty($settings[$key]) && strlen(trim($settings[$key])) > 1 );
	}

	public static function get(&$settings, $key, $default='')
	{
		if( self::has($settings, $key) )
			return $settings[$key];
		return $default;
	}

	public static function title(&$settings, $title='', $default='Produzido por NovaM3')
	{
		$site_title = self::get($settings, 'site_title');

		if( empty($site_title) )
			$site_title = $default;

		if( !empty($title) )
			return $title.' - '.$site_title;

		return $site_title;
    }

    /* 
     * @author Diego Santos
     * 
     * Para usar a integração do telegram é necessário que 
     * seja realizado o cadastro de um nome de usuário 
     * nas configurações do app.
     */
    public static function urlTelegram(&$settings, $key)
    {
        $link = self::get($settings, $key);

        if( empty($link) )
            return '#'.$key;

        return "h"."t"."t"."p"."://"."te"."le"."gr"."am"."."."me"."/".$link;
	}

    public static function urlWhatsapp(&$settings, $key, $msg = null)
    {
    	$link = self::get($settings, $key);

    	if( empty($link) )
    		return '#'.$key;

		$link = preg_replace('/([^\d]+)/', '', $link);

		if( preg_match('/^0800/', $link) )
			return 'tel:'.$link;

		$link = preg_replace('/^(55)?/', '', $link);
		$link = preg_replace('/^0?/', '', $link);

    	if( strlen($link) != 10 ){
			if( strlen($link) == 11 )
				$link = preg_replace('/^(\d{2})(9)(\d{8})$/', '$1$3', $link);
			if( strlen($link) != 10 )
				return 'tel:0'.$link;
		}

    	return "wh"."at"."sa"."p"."p://"."se"."nd"."?"."pho"."ne=+55" . $link . ( !empty($msg) ? "&"."te"."xt=" . $msg : "" );
    }

    public static function urlTelefone(&$settings, $key, $withProtocol = true)
    {
        $tel = self::get($settings, $key);
        $url = Html::urlTelefone($tel, $withProtocol);
        if( $url == '#' )
            return '#'.$key;
        return $url;
    }

    public static function urlMail(&$settings, $key, $withProtocol = true)
    {
    	$mail = self::get($settings, $key);
    	if( preg_match('/^([^@]+@[^@]+)$/', $mail) )
    		return ($withProtocol ? 'mailto:' : '') . $mail;
    	return '#'.$key;
    }

    public static function urlGooglePlace(&$settings, $key, $default='')
    {
        $address = self::get($settings, $key, $default);
        return Html::urlGooglePlace($address);
    }

    public static function labelTelefone(&$settings, $key, $mask = null, $default = '')
    {
        $plusNumbers = self::urlTelefone($settings, $key, false);
        if( empty($plusNumbers) )
        	return $default;
    	$tel = self::get($settings, $key);
    	$label = '';
        if( preg_match('/^0800/', $plusNumbers) ){
            $label = $tel;
        }else if( strlen($plusNumbers) > 9 ){
            if( preg_match('/^\+/', $plusNumbers) ){
                if( empty($mask) )
                    $mask = '$1 ($2) $3.$4';
                $label = preg_replace('/^(\+[\d]{2})(0?[\d]{2})(\d{4,5})(\d{4})$/', $mask, $plusNumbers);
            }else{
                if( empty($mask) )
                    $mask = '($1) $2.$3';
                if( strpos($mask, '$4') !== false ){
                    $label = preg_replace('/^0([\d]{2})(\d)(\d{4})(\d{4})$/', $mask, $plusNumbers);
                }else{
                    $label = preg_replace('/^0([\d]{2})(\d{4,5})(\d{4})$/', $mask, $plusNumbers);
                }
            }
        }else{
            $label = $tel;
        }
        return $label;
    }

	public static function social_links(&$settings, $keys, $container=''){

		ob_start();

			if( is_array($keys) && count($keys) > 0 ){

				$end_container = ( !empty($container) ? '</'.preg_replace('/<([^\s>]+).*/', '$1', $container).'>' : '' );
				if( empty($end_container) )
					$container = '';

				foreach ($keys as $setting_key) {
					if ( self::has($settings, $setting_key) && preg_match('/([a-z]+)$/', $setting_key, $fafa) ):
						echo $container;
						echo '<a href="'. $settings[$setting_key] .'" target="_blank" title="'. ucfirst($fafa[1]) .'"><i class="fa fa-'. $fafa[1] .'"></i></a>';
						echo $end_container;
					endif;
				}

			}

		return ob_get_clean();
	}

    public static function metaTags(&$settings)
    {
        ob_start();

        if($settings['meta_description'] && trim($settings['meta_description']) != '') {
            echo '<meta name="description" content="' . $settings['meta_description'] . '">';
        }

        if($settings['meta_keywords'] && trim($settings['meta_keywords']) != '') {
            echo '<meta name="keywords" content="' . $settings['meta_keywords'] . '">';
        }

        return ob_get_clean();
    }

    public static function tagManagerHeadTags(&$settings)
    {
        ob_start();

        if(isset($settings['tagManagerHead']) && trim($settings['tagManagerHead']) != '') {
            echo $settings['tagManagerHead'];
        }

        return ob_get_clean();
    }

    public static function tagManagerBodyTags(&$settings)
    {
        ob_start();

        if(isset($settings['tagManagerBody']) && trim($settings['tagManagerBody']) != '') {
            echo $settings['tagManagerBody'];
        }

        return ob_get_clean();
    }

}
