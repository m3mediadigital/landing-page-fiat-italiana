<head>

	<title>{{ $settings['site_title'] or  env("APP_NAME", "SITE") }} - @yield('title')</title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="{{ $settings['meta_description'] or '' }}">
	<meta name="keywords" content="{{ $settings['meta_keywords'] or '' }}">
	<meta name="author" content="Nova M3">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css">
	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700,800,900" rel="stylesheet">

	<link href="{{ asset('images/favicon.ico') }}" rel="shortcut icon" type="image/x-icon">

	<link rel="canonical" href="{{ $settings['website_url'] or env("APP_URL", "SITE") }}" />

<!-- Share Tags -->
@yield('share')

</head>
