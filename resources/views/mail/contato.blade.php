@if( !empty($requestData['nome']) )

    <p></p>

    <p><strong>Nome:</strong><br/> {{ $requestData['nome'] }}</p>

    @if( !empty($requestData['email']) )
        <p><strong>Email:</strong><br/> {{ $requestData['email'] }}</p>
    @endif

    @if( !empty($requestData['telefone']) )
        <p><strong>Telefone:</strong><br/> {{ $requestData['telefone'] }}</p>
    @endif

@endif
@if( !empty($requestData['tipo_veiculo']) )
    <p><strong>Tipo veículo:</strong><br/> {{ $requestData['tipo_veiculo'] }}</p>
@endif
@if( !empty($requestData['origem']) )
    <p><strong>Origem:</strong><br/> {{ $requestData['origem'] }}</p>
@endif
@if( !empty($requestData['atendimento']) )
    <p><strong>Atendimento:</strong><br/> {{ $requestData['atendimento'] }}</p>
@endif
@if( !empty($requestData['celular']) )
    <p><strong>Celular:</strong><br/> {{ $requestData['celular'] }}</p>
@endif

@if( !empty($requestData['cpf']) )
    <p><strong>CPF:</strong><br/> {{ $requestData['cpf'] }}</p>
@endif

@if( !empty($requestData['cargo']) )
    <p><strong>Cargo:</strong><br/> {{ $requestData['cargo'] }}</p>
@endif

@if( !empty($requestData['assunto']) )
    <p><strong>Assunto:</strong><br/> {{ $requestData['assunto'] }}</p>
@endif
@if( !empty($requestData['solicitacao']) )
    <p><strong>Solicitação de:</strong><br/> {{ $requestData['solicitacao'] }}</p>
@endif
@if( !empty($requestData['descricao_peca']) )
    <p><strong>Descrição da peça/acessório:</strong><br/> {{ $requestData['descricao_peca'] }}</p>
@endif
@if( !empty($requestData['chassi']) )
    <p><strong>Numeração do Chassi:</strong><br/> {{ $requestData['chassi'] }}</p>
@endif
@if( !empty($requestData['ano_modelo']) )
    <p><strong>Ano/Modelo:</strong><br/> {{ $requestData['ano_modelo'] }}</p>
@endif
@if( !empty($requestData['combustivel']) )
    <p><strong>Combustível:</strong><br/> {{ $requestData['combustivel'] }}</p>
@endif


@if( !empty($requestData['item']) )
    <p><strong>Interesse:</strong><br/> {{ $requestData['item'] }}</p>
@endif

@if( !empty($requestData['modelos_compativeis']) )
    <p><strong>Modelos Compatíveis:</strong><br/> {{ $requestData['modelos_compativeis'] }}</p>
@endif



@if( !empty($requestData['tipo']) )
    <p><strong>Venda Direta:</strong><br/> {{ $requestData['tipo'] }}</p>
@endif



@if( !empty($requestData['modelo']) )
    @if( !empty($requestData['fabricante']) )
        <p><strong>Marca:</strong><br/> {{ $requestData['fabricante'] }}</p>
    @endif

    <p><strong>Modelo:</strong><br/> {{ ucfirst($requestData['modelo']) }}</p>

    @if( !empty($requestData['ano']) )
        <p><strong>Ano:</strong><br/> {{ $requestData['ano'] }}</p>
    @endif
@endif



@if( !empty($requestData['veiculo']) )
    <p><strong>Veículo:</strong><br/> {{ $requestData['veiculo'] }}</p>
@endif

@if( !empty($requestData['versao']) )
    <p><strong>Versão:</strong><br/> {{ $requestData['versao'] }}</p>
@endif

@if( !empty($requestData['placa']) )
    <p><strong>Placa:</strong><br/> {{ $requestData['placa'] }}</p>
@endif

@if( !empty($requestData['forma_pagamento']) )
    <p><strong>Forma de Pagamento:</strong><br/> {{ $requestData['forma_pagamento'] }}</p>
@endif
@if( !empty($requestData['emplacar']) )
    <p><strong>Emplacamento:</strong><br/> {{ $requestData['emplacar'] }}</p>
@endif
@if( !empty($requestData['parcelas']) )
    <p><strong>Parcelas:</strong><br/> {{ $requestData['parcelas'] }}</p>
@endif
@if( !empty($requestData['valor_parcela']) )
    <p><strong>Valor Calculado das parcelas:</strong><br/> {{ $requestData['valor_parcela'] }}</p>
@endif


@if( !empty($requestData['entrada']) )
    <p><strong>Entrada:</strong><br/> {{ $requestData['entrada'] }}</p>
@endif

@if( !empty($requestData['revisao']) )
    <p><strong>Revisao:</strong><br/> {{ $requestData['revisao'] }} Km</p>
@endif

@if( !empty($requestData['dia']) && !empty($requestData['horario']) )
    <p><strong>Agendamento:</strong><br/> {{ $requestData['dia'] }} às {{ $requestData['horario'] }}</p>
@endif

@if( !empty($requestData['dia_2']) && !empty($requestData['horario_2']) )
    <p><strong>Agendamento - Opção 02:</strong><br/> {{ $requestData['dia_2'] }} às {{ $requestData['horario_2'] }}</p>
@endif


@if( !empty($requestData['data_1']) && !empty($requestData['horario_1']) )
    <p><strong>Agendamento - Opção 01:</strong><br/> {{ $requestData['data_1'] }} às {{ $requestData['horario_1'] }}</p>
@endif

@if( !empty($requestData['data_2']) && !empty($requestData['horario_2']) )
    <p><strong>Agendamento - Opção 02:</strong><br/> {{ $requestData['data_2'] }} às {{ $requestData['horario_2'] }}</p>
@endif

@if( !empty($requestData['servico']) )
    <p><strong>Serviço:</strong><br/> {{ $requestData['servico'] }}</p>
@endif


@if( !empty($requestData['anexos']) && is_array($requestData['anexos']) )
    <p>
        <strong>Anexos:</strong><br/>
    <ul>
        @foreach($requestData['anexos'] as $anexo)
            <li>
                {!! $anexo !!}
            </li>
        @endforeach
    </ul>
    </p>
@endif


@if( !empty($requestData['loja']) )
    <p><strong>Loja:</strong><br/> {{ ucfirst($requestData['loja']) }}</p>
@endif

@if( !empty($requestData['mensagem']) )
    <p><strong>Mensagem:</strong><br/> {{ ucfirst($requestData['mensagem']) }}</p>
@endif

@if( !empty($requestData['observacoes']) )
    <p><strong>Observações:</strong><br/> {{ ucfirst($requestData['observacoes']) }}</p>
@endif


