<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contato extends Mailable
{
    use Queueable, SerializesModels;

    public $requestData;
    public $layout;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($requestData, $layout = null)
    {
        $this->requestData = !empty($requestData) ? $requestData : [];
        $this->layout = !empty($layout) ? $layout : 'mail.contato';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
        if( !empty($this->layout) )
            return $this->view($this->layout);
        return $this->view('mail.contato');
    }
}
