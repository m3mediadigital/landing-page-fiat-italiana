<?php

namespace App\Helpers;

/**
* 
*/
class Html extends AppHelper
{
    /* 
     * @author Diego Santos
     * 
     * Para usar a integração do telegram é necessário que 
     * seja realizado o cadastro de um nome de usuário 
     * nas configurações do app.
     */
    public static function urlTelegram($link)
    {
        return "h"."t"."t"."p"."://"."te"."le"."gr"."am"."."."me"."/".$link;
	}

    public static function urlWhatsapp($link, $msg = null)
    {
    	$link = self::get($settings, $key);

    	if( !empty($link) ){

            $link = preg_replace('/([^\d]+)/', '', $link);

            if( preg_match('/^0800/', $link) )
                return 'tel:'.$link;

            $link = preg_replace('/^(55)?/', '', $link);
            $link = preg_replace('/^0?/', '', $link);

            if( strlen($link) != 10 ){
                if( strlen($link) == 11 )
                    $link = preg_replace('/^(\d{2})(9)(\d{8})$/', '$1$3', $link);
                if( strlen($link) != 10 )
                    return 'tel:0'.$link;
            }

            return "wh"."at"."sa"."p"."p://"."se"."nd"."?"."pho"."ne=+55" . $link . ( !empty($msg) ? "&"."te"."xt=" . $msg : "" );
        
        }

		return '#';
    }

    public static function urlTelefone($tel, $withProtocol = true)
    {
    	$plusNumbers = preg_replace('/([^\d\+]+)/', '', $tel);
        if( !empty($plusNumbers) ){
        	if( !preg_match('/^0800/', $plusNumbers) ){
                if( strlen($plusNumbers) > 9 ){
                	if( preg_match('/^\+/', $plusNumbers) ){
    	                $plusNumbers = preg_replace('/^(\+\d{2})(0?)/', '$1', $plusNumbers);
    	            }else{
    	                if( !preg_match('/^0(\d+)$/', $plusNumbers) )
    	                    $plusNumbers = '0'.$plusNumbers;
    	            }
    	        }
            }
    	   return ( ($withProtocol ? 'tel:' : '' ) . $plusNumbers );
        }
        return '#';
    }

    public static function urlMail($mail, $withProtocol = true)
    {
    	if( preg_match('/^([^@]+@[^@]+)$/', $mail) )
    		return ($withProtocol ? 'mailto:' : '') . $mail;
    	return '#';
    }

    public static function urlGooglePlace($address, $default='')
    {
        return 'https://www.google.com.br/maps/place/'.($address);
    }

}
