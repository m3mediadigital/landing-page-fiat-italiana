<!DOCTYPE html>
<html lang="pt-br">
@include('pages.layout.head')
<body>
    <div class="body-container">

        @yield('content')

        {{-- @include('pages.layout.m3') --}}

        @include('pages.layout.scripts')
    </div>
</body>
</html>